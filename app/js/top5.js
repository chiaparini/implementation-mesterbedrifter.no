function getTop5( fag_filter )
{
	var $loading = $('.ratingList .loading');
	$loading.fadeIn( function()
	{
		var ajax_filters = 
		{
			filters : fag_filter,

			size    : 5,
			from    : 0,

			fb      : 'should'
		};

		if( typeof fag_filter != 'undefined' )
		{
			ajax_filters.filters = fag_filter;
		}

		var global_geolocation = getCookie('geolocation');
		if( global_geolocation != '' )
		{
			ajax_filters.places = [ ( global_geolocation.split(',')[3] ).toUpperCase() ];
		}

		$.get
		(
			'/@mb',
			ajax_filters,
			function( response )
			{
				// Result list
				var $result_list = $('.ratingList ul');
				$result_list.html('');

				var total_results = response.result.hits.total;
				if( total_results == 0 )
				{
					$('.ratingList .no-results').show();
				}
				else
				{
					$('.ratingList .no-results').hide();

					// Populates the result list
					for( var item = 0; item < 5; item++ )
					{
						var obj = response.result.hits.hits[item];

						if( typeof obj == 'undefined' )
						{
							break;
						}
						
						$result_list.append( composeTop5Item( obj, item + 1 ) );
					}
				}

				$loading.fadeOut();
			}
		);
	});
};


function composeTop5Item( result, item )
{
	var new_item = '<li class="clearfix">';
	new_item += '<div class="companyName"><a href="/bedrift/'+ result._id +'/'+ slugify( result._source.name ) +'">'+ item +'. <strong>'+ result._source.name +'</strong></a></div>';

	new_item += composeResultItemRating( result._source.rating );

	new_item += '<div class="companyCategory"><a href="#" class="linkIcon">'+ result._source.address1 +'</a></div>';
	new_item += '</li>';

	return new_item;
}


// 'Finn' button inside the 'Velg lokasjon' modal window: finds the nearest kommune based on
// the postnummer inputted by the user
$('#findLatLongByPostnummer_btn').click( function(e)
{
	e.preventDefault();

	var postnummer = $('#locationModal [name="postnr"]').val();

	if( postnummer != '' )
	{
		var $loading = $('#locationModal .loading');
		$loading.fadeIn( function()
		{
			$.get
			(
				'/@mb/pn',
				{
					zip: postnummer
				},
				function( response )
				{
					if( response.info )
					{
						$('#locationModal select[name="sted"]').val( response.info.commune );
						$('#locationModal select[name="kommune"]').val( response.info.county_name.toUpperCase() );
					}
					else
					{
						$('#findLatLongByPostnummer_btn')
							.val('Ingen treff')
							.addClass('ajax-error');

						window.setTimeout( function()
						{
							$('#findLatLongByPostnummer_btn')
								.val('Finn')
								.removeClass('ajax-error');
						}, 3000);
					}
				}
			);
		});

		$loading.fadeOut();
	}
});

$('#locationModal [name="postnr"]').keypress( function(e)
{
	if( e.keyCode == 13 )
	{
		$('#findLatLongByPostnummer_btn').click();
	}
});

// On every change of the 'Kommune' <select> field, fetch on Google for Lat and Long using the selected Kommune
$('#locationModal select[name="kommune"]').change( function()
{
	var kommune = $(this).find('option:selected').text();

	var $loading = $('#locationModal .loading');
	$loading.fadeIn( function()
	{
		$.get
		(
			'https://maps.googleapis.com/maps/api/geocode/json?c‌​omponents=country:Norway&address='+ kommune,
			{},
			function( response )
			{
				var loc = response.results[0].geometry.location;
				$('#chooseNewLocation_btn').attr
				({
					'data-lat': loc.lat,
					'data-lon': loc.lng
				});

				$loading.fadeOut();
			}
		);
	});
});

// Final button inside the Change Location modal window
$('#chooseNewLocation_btn').click( function(e)
{
	setCookie('geolocation', $(this).attr('data-lat') +','+ $(this).attr('data-lon') +','+ $('#locationModal select[name="sted"]').val() +','+ $('#locationModal select[name="kommune"] option:selected').text(), 1);

	$('select[name="areas"]').val( $('#locationModal select[name="sted"]').val() );
	$('select[name="places"]').val( $('#locationModal select[name="kommune"]').val() );
	
	getTop5();
	$('.ratingList h4 strong').text( $('#locationModal select[name="kommune"] option:selected').text() );
	$('#locationModal .close-reveal-modal').click();
	scrollWindowTo( $('#frontContent .ratingList') );
});

$('#frontContent .ratingList h4 a').click( function()
{
	$('#locationModal').foundation('reveal', 'open');
});