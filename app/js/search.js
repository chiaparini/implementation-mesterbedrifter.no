var first_ajax = true;
var page_fag_filter = undefined;

function triggerSearch( top_filter, target_pagination_page, callback )
{
	var $loading = $('.loading');
	$loading.fadeIn( function()
	{
		var s = $('.textSearch input').val();

		var fag_filter     = [],
		    sted_filter    = [],
		    kommune_filter = [];

		if( top_filters )
		{
			var top_filters = $('.advancedSearch');

			fag_filter     = [ top_filters.find('select[name="filters"]').val() ];
			sted_filter    = [ top_filters.find('select[name="areas"]').val() ];
			kommune_filter = [ top_filters.find('select[name="places"]').val() ];
		}

		if( typeof page_fag_filter == 'undefined' )
		{
			$('.fag_filter_list :checkbox:checked').each( function( i, el )
			{
				fag_filter.push( $(el).val() );
			});
		}
		else
		{
			fag_filter = [ page_fag_filter ];
		}
		
		$('.omrader_filter_list > li > label :checkbox:checked').each( function( i, el )
		{
			sted_filter.push( $(el).val() );
		});

		$('.omrader_filter_list ul :checkbox:checked').each( function( i, el )
		{
			kommune_filter.push( $(el).val() );
		});

		var certs = $('.certification_filter_list :checkbox');

		var must_or_should = top_filter ? 'must' : 'should';

		// default page is 1
		if( typeof target_pagination_page == 'undefined' )
		{
			target_pagination_page = 1;
		}

		// Main filters
		var ajax_filters = 
		{
			q              : s,

			filters        : fag_filter,
			areas          : sted_filter,
			places         : kommune_filter,

			size           : 10,
			from           : ( parseInt(target_pagination_page) - 1 ) * 10,

			fb             : must_or_should,
			ab             : must_or_should,
			pb             : must_or_should
		};

		// Certification filters
		if( certs.eq(0).is(':checked') )
		{
			ajax_filters.certification1 = true;
		}
		if( certs.eq(1).is(':checked') )
		{
			ajax_filters.certification2 = true;
		}
		if( certs.eq(2).is(':checked') )
		{
			ajax_filters.certification3 = true;
		}

		// Sorting
		var sorting_param = '';
		var sorting_select = $('#SortSelect').val();
		if( sorting_select != '' )
		{
			if( sorting_select.indexOf('dist') != -1 )
			{
				// GEO Location
				var global_geolocation = getCookie('geolocation');
				if( global_geolocation != '' )
				{
					ajax_filters.geo =
					{
						lat: global_geolocation.split(',')[0],
						lon: global_geolocation.split(',')[1]
					}
				}
			}
			else
			{
				var sorting_info = sorting_select.split(':');

				var order_by_field = sorting_info[0];
				var direction      = sorting_info[1];

				sorting_param = '?order['+ order_by_field +']='+ direction;
			}
		}

		$.get
		(
			'/@mb'+ sorting_param,
			ajax_filters,
			function( response )
			{
				// Result list
				var $result_list = $('#listContainer.search-results-ul');
				$result_list.html('');

				var total_results = response.result.hits.total;
				if( total_results == 0 )
				{
					$('.no-results').show();
				}
				else
				{
					$('.no-results').hide();


					// Pagination
					if( total_results > 10 )
					{
						$('.pagination li:not(.arrow)').remove();

						var i;
						for( i = 1; i <= ( total_results / 10 ); i++ )
						{
							$('.pagination .arrow:last').before( $('<li'+ ( ( i == target_pagination_page ) ? ' class="current"' : '' ) +'><a href="#" data-target-page="'+ i +'">'+ i +'</a></li>') );
						}

						if( total_results % 10 != 0 )
						{
							$('.pagination .arrow:last').before( $('<li'+ ( ( i == target_pagination_page ) ? ' class="current"' : '' ) +'><a href="#" data-target-page="'+ i +'">'+ i +'</a></li>') );
						}

						if( target_pagination_page == 1 )
						{
							$('.pagination .arrow:first').addClass('unavailable');
						}
						else
						{
							$('.pagination .arrow:first').removeClass('unavailable');
						}

						if( ( total_results % 10 != 0 && target_pagination_page == i ) || (  total_results % 10 == 0 && target_pagination_page == i-1 ) )
						{
							$('.pagination .arrow:last').addClass('unavailable');
						}
						else
						{
							$('.pagination .arrow:last').removeClass('unavailable');
						}

						$('.pagination .arrow:first').find('a').attr('data-target-page', parseInt(target_pagination_page) - 1);
						$('.pagination .arrow:last a').attr('data-target-page', parseInt(target_pagination_page) + 1);

						$('.pagination-container').show();
					}
					else
					{
						$('.pagination-container').hide();
					}

					// Populates the result list
					var total_items_to_display = response.result.hits.total;
					total_items_to_display = ( total_items_to_display > 10 ) ? 10 : total_items_to_display;
					for( var item = 0; item < total_items_to_display; item++ )
					{
						var obj = response.result.hits.hits[item];

						if( typeof obj == 'undefined' )
						{
							break;
						}

						$result_list.append( composeResultItem( obj ) );
					}
					$result_list.find('> li').show();

					// Updates filters
					updateFilters( response.result, ajax_filters );


					// Scroll page to top if on Search Results page
					if( $('.search-results-page').length )
					{
						$('html, body').animate
						({
							scrollTop: 0
						}, 500);
					}
				}


				if( typeof callback != 'undefined' )
				{
					callback( response );
				}


				// ... after everything is done
				first_ajax = false;
				$('.search-query').text( s );
				$loading.fadeOut();
			}
		);
	});
};


$('.standardSearch a').click( function(e)
{
	e.preventDefault();

	triggerSearch( true );
});

$('form.search-form.prevent-submit').submit( function(e)
{
	e.preventDefault();

	triggerSearch( true );
});

$('.search-results-page .filterList').on('change', ':checkbox', function()
{
	triggerSearch( false );
});
$('.use-search-callback .filterList').on('change', ':checkbox', function()
{
	triggerSearch( true, 1, function( ajax_response )
	{
		var total_results = ajax_response.result.hits.total;
		$('.searchResultList .filterBtns').show()
			.find('strong').text( total_results +' bedrift'+ ( ( total_results != 1 ) ? 'er' : '' ) );
	});
});

$('.pagination').on('click', 'a', function()
{
	triggerSearch( false, $(this).attr('data-target-page') );
});

// when the Search Results page loads: transform GET params into AJAX
var q = findGetParameter('q');
if( q !== null )
{
	$('.textSearch input').val( q );
	triggerSearch( true );
}

// Search sorting
$('#SortSelect').change( function()
{
	var sorting = $(this).val();

	triggerSearch( true, 1, function( ajax_response )
	{
		var total_results = ajax_response.result.hits.total;
		$('.searchResultList .filterBtns').show()
			.find('strong').text( total_results +' bedrift'+ ( ( total_results != 1 ) ? 'er' : '' ) );
	});
});


function composeResultItem( result )
{
	var package_type_1 = result._source.package1;
	var package_type_2 = result._source.package2;
	var package_type_3 = result._source.package3;

	var new_item = '';
	var category = '';

	var item_link = '/bedrift/'+ result._id +'/'+ slugify( result._source.name );

	if( package_type_1 )
	{
		category = '1';

		// first row
		new_item += '<div class="row"><div class="large-8 columns listTextBox"><h3><a href="'+ item_link +'">'+ result._source.name +'</a></h3>';

		new_item += composeResultItemRating( result._source.rating );

		new_item += '<p>'+ ( result._source.description || '' ) +'</p></div><div class="large-4 columns listBrandBox"><div class="logoBox"><a href="'+ item_link +'"><img src="'+ ( result.image || '' ) +'" /></a></div>';

		new_item += composeResultItemCertLogos( result._source );

		new_item += '</div></div>';

		// second row
		new_item +='<div class="row"><div class="large-3 columns listContactBox"><p class="bold">'+ result._source.address1 +'<br />Kommune: '+ result._source.commune +' / Bydel: '+ result._source.place +'<br />Tlf: '+ result._source.phone +'</p></div><div class="large-9 columns listServicesBox"><strong>Våre tjenester</strong><ul>';

		var services = result._source.subjects;
		for ( var i = 0; i < services.length; i++ )
		{
			new_item += '<li><i class="fa fa-angle-right" aria-hidden="true"></i> '+ services[i].trim() +'</li>'
		}
		new_item += '</ul></div></div>';

		/* NO IMAGES CURRENTLY COME WITH AJAX. ALEXEY WILL CHECK ON THIS
		// third row
		var gallery = result.image_gallery;
		if( gallery.length > 0 )
		{
			new_item += '<div class="row"><div class="columns listGalleryBox"><h5>Bildegalleri:</h5><ul>';

			for ( var i = 0; i < gallery.length; i++ )
			{
				var image = gallery[i];
				new_item += '<li><div class="thumbBox"><a href="/media/'+ image +'" class="fancybox" rel="gallery_'+ image.id +'"><img src="/media/'+ image +'" alt="galleribilde" /></a></div></li>';
			}

			new_item += '</ul></div></div>';
		}*/
	}

	if( package_type_2 )
	{
		category = '2';

		new_item += '<div class="large-8 columns listTextBox"><h4><a href="'+ item_link +'">'+ result._source.name +'</a></h4>';

		new_item += composeResultItemRating( result._source.rating );

		new_item +=  '<p>'+ result._source.description +'</p>';

		new_item += '</div><div class="large-4 columns listBrandBox">';

		new_item += composeResultItemCertLogos( result._source );

		new_item += '</div></div><div class="row"><div class="large-12 columns listContactBox">';

		new_item += '<p class="bold">'+ result._source.address1 +', '+ result._source.commune +', Tlf: '+ result._source.phone +'</p></div></div>';
	}

	// We didn't use an 'else' clause because those 3 Package-related fields are checkboxes on the
	// DNA panel, therefore we need to prevent errors here
	if( ( ! package_type_1 && ! package_type_2 ) || package_type_3 )
	{
		category = '3';

		new_item += '<div class="row"><div class="large-8 columns listTextBox"><h5><a href="'+ item_link +'">'+ result._source.name +'</a></h5>';
		new_item += '<p>'+ result._source.address1 +', '+ result._source.commune +'</p>';

		new_item += composeResultItemRating( result._source.rating );

		new_item += '</div><div class="large-4 columns listBrandBox">';

		new_item += composeResultItemCertLogos( result._source );

		new_item += '</div></div>';
	}

	var new_item_header = '<li class="mix" data-rating="'+ result._source.rating +'" data-dist="0" data-alfa="'+ result._source.name +'"><div class="listBox category'+ category +' clearfix"><div class="columns">';
	var new_item_closing_tags = '</div></li>';

	return $(new_item_header + new_item + new_item_closing_tags);
}

function composeResultItemRating( rating )
{
	var html = '<div class="companyRating">';
	var icon_class = '';
	for( var i = 0; i < 5; i++ )
	{
		if( i < rating )
		{
			icon_class = 'thumbs-up';
		}
		else
		{
			icon_class = 'thumbs-o-up';
		}

		html += '<i class="fa fa-'+ icon_class +'" aria-hidden="true"></i>'
	}
	html += '</div>';

	return html;
}

function composeResultItemCertLogos( obj )
{
	var html = '<div class="certificationLogos"><ul><li><a href="#"><img src="/media/dist/images/logoer/';

	if( obj.certification1 )
	{
		html += 'mesterlogo.png" alt="Mesterbedrift"';
	}
	if( obj.certification2 )
	{
		html += 'ansvarsrett_farge-333.png" alt="Godkjent for ansvarsrett"';
	}
	if( obj.certification3 )
	{
		html += 'GodkjentVaatromsbedrift.jpg" alt="Godkjent Våtromsbedrift"';
	}

	html += ' /></a></li></ul></div>';
	return html
}

function updateFilters( data, ajax_filters )
{
	// 'Sertifiseringer' filters (fixed)
	var c1;
	for( var i = 0; i < data.aggregations.mesterbrev.buckets.length; i++ )
	{
		if( data.aggregations.mesterbrev.buckets[i].key_as_string == 'true' )
		{
			c1 = data.aggregations.mesterbrev.buckets[i].doc_count;
		}
	}
	$('.certification_filter_list li').eq(0).html('<label><input type="checkbox" name="certification1" '+ ( ajax_filters.certification1 ? 'checked="checked"' : '' ) +' /> Sentral godkjenning ('+ c1 +')</label>');
	var c2;
	for( var i = 0; i < data.aggregations.dibk.buckets.length; i++ )
	{
		if( data.aggregations.dibk.buckets[i].key_as_string == 'true' )
		{
			c2 = data.aggregations.dibk.buckets[i].doc_count;
		}
	}
	$('.certification_filter_list li').eq(1).html('<label><input type="checkbox" name="certification2" '+ ( ajax_filters.certification2 ? 'checked="checked"' : '' ) +' /> Godkjent våtromsbedrift ('+ c2 +')</label>');
	/*var c3;
	for( var i = 0; i < data.aggregations.fvv.buckets.length; i++ )
	{
		if( data.aggregations.fvv.buckets[i].key_as_string == 'true' )
		{
			c3 = data.aggregations.fvv.buckets[i].doc_count;
		}
	}
	$('.certification_filter_list li').eq(2).html('<input type="checkbox" name="certification3" '+ ( ( ajax_filters.certification3 ? 'checked="checked"' : '' ) +' /> NAME_HERE ('+ c3 +')');*/

	// 'Bransjer' filters
	$('.fag_filter_list').html('');
	var fag_response = data.aggregations.agg_subjects.buckets;
	for( var i = 0; i < fag_response.length; i++ )
	{
		var item = fag_response[i];
		if( item.doc_count > 0 )
		{
			$('.fag_filter_list').append( $('<li><label><input type="checkbox" value="'+ item.key +'" name="filters[]" '+ ( ( ajax_filters.filters.indexOf( item.key ) != -1 ) ? 'checked="checked"' : '' ) +' /> '+ item.key +' ('+ item.doc_count +')</label>') );
		}
	}

	// 'Områder' filters
	$('.omrader_filter_list').html('');
	var omrader_response = data.aggregations.places.buckets;
	for( var i = 0; i < omrader_response.length; i++ )
	{
		var item = omrader_response[i];
		if( item.doc_count > 0 )
		{
			var $filter_item = $('<li><label><input type="checkbox" value="'+ item.key +'" name="filters[]" '+ ( ( ajax_filters.areas.indexOf( item.key ) != -1 ) ? 'checked="checked"' : '' ) +' /> '+ item.key +' ('+ item.doc_count +')</label>');
		}

		var $sublist = $('<ul></ul>');
		var places = item.places.buckets;
		for( var j = 0; j < places.length; j++ )
		{
			var sub_item = places[j];
			if( sub_item.doc_count > 0 )
			{
				$sublist.append( $('<li><label><input type="checkbox" value="'+ sub_item.key +'" name="filters[]" '+ ( ( ajax_filters.places.indexOf( sub_item.key ) != -1 ) ? 'checked="checked"' : '' ) +' /> '+ sub_item.key +' ('+ sub_item.doc_count +')</label>') );
			}
		}

		$filter_item.append( $sublist );
		$('.omrader_filter_list').append( $filter_item );
	}
}

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];
	location.search
	.substr(1)
		.split("&")
		.forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

function slugify(Text)
{
	return Text
		.toLowerCase()
		.replace(/[^\w ]+/g,'')
		.replace(/ +/g,'-')
		;
}