$(window).trigger('resize').trigger('scroll');

$(function(){
  var $filterSelect = $('#FilterSelect'),
	  $sortSelect = $('#SortSelect'),
	  $container = $('#listContainer:not(.search-results-ul)');
  
  $container.mixItUp();
  
  $filterSelect.on('change', function(){
	$container.mixItUp('filter', this.value);
  });
  
  $sortSelect.on('change', function(){
	$container.mixItUp('sort', this.value);
  });
});


$(document).ready(function(){
	
		
	$(".js-select2-multiple").select2();
	
	var cloneCount = 1;
	$("a.addListItem").click(function(){
	  $("#dep").clone().attr('id', 'id'+ cloneCount++).insertAfter("#dep");
	});
	
	$("a.addServiceItem").click(function(){
	  $("#serviceItem").clone().attr('id', 'id'+ cloneCount++).insertAfter("#serviceItem");
	});

	$('#content').redactor();
	
	$("#uploadTrigger").click(function(){
		$("#uploadFile").click();
	});
	

	if( $('#slideshow').length > 0 && !$('#slideshow').hasClass('static') ) {
		$('#slideshow').argoslide({
			navIconPrev: 'fa fa-angle-left',
			navIconNext: 'fa fa-angle-right'
		});
	}

	$('.menuToggle').on('click', function(){
		$('#mainNav').toggleClass('open');
	})
	
	$('#searchToggle').on('click', function(){
		$('.advancedSearch').toggleClass('open');
		$('#searchToggle').toggleClass('active');
	})
	
	$('.moreRatingsToggle a').on('click', function(){
		$('#companyRatings').toggleClass('open');
		$('.moreRatingsToggle').toggleClass('active');
	})
	
	$('.readMoreToggle a').on('click', function(){
		$('.contentBox').toggleClass('open');
		$('.linkBox').toggleClass('active');
	})
	
	$('.companyServicesList>.defaultServices>ul>li>a.moreToggle, .companyServicesList>.customServices>ul>li>a.moreToggle').click(function(){
		var elem = $(this).parent().children('.serviceInfoBox');
		
		//Check if this one is allready open
		if(elem.hasClass('open')){
			elem.removeClass('open');
			
		} else {
			//It is not already open
			$('.companyServicesList>.defaultServices>ul>li>div.serviceInfoBox, .companyServicesList>.customServices>ul>li>div.serviceInfoBox').removeClass('open');
			elem.addClass('open');
			
		}
	});
	
	
	$('#branchList li#1 a.moreToggle').on('click', function(){
		$('#branchList li#1 ul').toggleClass('open');
		$('#branchList li#1 a.moreToggle').toggleClass('active');
	})
	
	$('#branchList li#2 a.moreToggle').on('click', function(){
		$('#branchList li#2 ul').toggleClass('open');
		$('#branchList li#2 a.moreToggle').toggleClass('active');
	})
	
	$('#branchList li#3 a.moreToggle').on('click', function(){
		$('#branchList li#3 ul').toggleClass('open');
		$('#branchList li#3 a.moreToggle').toggleClass('active');
	})
	
	$('#branchList li#4 a.moreToggle').on('click', function(){
		$('#branchList li#4 ul').toggleClass('open');
		$('#branchList li#4 a.moreToggle').toggleClass('active');
	})
	
	$('#branchList li#5 a.moreToggle').on('click', function(){
		$('#branchList li#5 ul').toggleClass('open');
		$('#branchList li#5 a.moreToggle').toggleClass('active');
	})
	
	$('#branchList li#6 a.moreToggle').on('click', function(){
		$('#branchList li#6 ul').toggleClass('open');
		$('#branchList li#6 a.moreToggle').toggleClass('active');
	})
	$('#branchList li#7 a.moreToggle').on('click', function(){
		$('#branchList li#7 ul').toggleClass('open');
		$('#branchList li#7 a.moreToggle').toggleClass('active');
	})
	
	$('div.fadeIn').hide().delay(500).fadeIn(1000).removeClass('hidden');
	
	$('a[href$=".pdf"]').addClass("pdf");
	$('a[href$=".doc"]').addClass("doc");
	$('a[href$=".docx"]').addClass("doc");
	$('a[href$=".xls"]').addClass("xls");
	$('a[href$=".xlsx"]').addClass("xls");
	
	
	$('#image-cropper').cropit({
	  imageBackground: true,
	  imageBackgroundBorderWidth: 15 // Width of background border
	});
	
	$('.select-image-btn').click(function() {
	  $('.cropit-image-input').click();
	});
	
	// Handle rotation
	$('.rotate-cw-btn').click(function() {
	  $('#image-cropper').cropit('rotateCW');
	});
	$('.rotate-ccw-btn').click(function() {
	  $('#image-cropper').cropit('rotateCCW');
	});

	
	
	// SLIDESHOW Bakgrunnsbilde
	$(function(){
		$("#frontBanner").each(function(){
		var d = $(this), i = d.find('img').first();
			if(i.length) {
				d.css({
					'background' : 'url(' + i.attr('src') + ') no-repeat'
				});
				i.remove();
			}
		});
	});
	
	
	// Scroll
	$(window).scroll(function(){
		var st = $(this).scrollTop();
		if( st >= 10){
			$('#top').addClass('minimized');
			$('#subpageBanner').addClass('minimized');
		}else{
			$('#top').removeClass('minimized');
			$('#subpageBanner').removeClass('minimized');
		}
	});
	// Add this class for when the website loads in the middle of the page
	if( $(window).scrollTop() >= 10)
	{
		$('#top').addClass('minimized');
		$('#subpageBanner').addClass('minimized');
	}
	
	$(window).scroll(function() {
		$(".scrollOut").css({
			'opacity' : 1-(($(this).scrollTop())/250)
		});         
	});
	
	$('.bxslider').bxSlider();
	
	$(function(){
		$('#listContainer:not(.search-results-ul)').mixItUp({
			controls: {
				enable: true
			},
			selectors: {
				filter: 'none' // tell MixItUp not to look for '.filter' buttons
			},
			load: {
				filter: 'all'
			}
		});  
	});
	
	function formatTitle(title, currentArray, currentIndex, currentOpts) {
	return '<div id="galleryCaption-title"><span><a href="javascript:;" onclick="$.fancybox.close();"><img src="/data/closelabel.gif" /></a></span>' + (title && title.length ? '<b>' + title + '</b>' : '' ) + 'Image ' + (currentIndex + 1) + ' of ' + currentArray.length + '</div>';
}

$(".galleryCaption").fancybox({
	'showCloseButton'	: false,
	'titlePosition' 		: 'inside',
	'titleFormat'		: formatTitle
});
	
	// Initierer fancybox om brukeren ikke er på touch skjerm
	if(!$('body').hasClass('touch')) {
	
		$('.fancybox').fancybox({
			padding: 10,
			scrolling: 'yes',
			nextEffect: 'fade',
			prevEffect: 'fade',
			helpers: {
				overlay: {
				  locked: false
				},
				title: {
					type: 'inside'
				}
			}
		});
	}

});


// Quicktech

// Ignore anchors with '#' only
$('body').on('click', 'a[href="#"]', function(e)
{
	e.preventDefault();
});

// Changing 'fylke' <select> elements display related 'Kommuner' only
$('.fylke_select').change( function()
{
	var fylke_name = $(this).val();
	var $options = $(this).parents('.advancedSearch').find('.kommune_select option');

	if( fylke_name == '' )
	{
		$options.show();
	}
	else
	{
		$options.hide();

		$options.each( function( i, el )
		{
			if( $(el).attr('data-for') == fylke_name )
			{
				$(el).show();
			}
		});
	}

	$options.parent().val('');
});
$('#locationModal select[name="sted"]').change( function()
{
	var fylke_name = $(this).val();
	var $options = $('#locationModal select[name="kommune"] option');

	if( fylke_name == '' )
	{
		$options.show();
	}
	else
	{
		$options.hide();

		$options.each( function( i, el )
		{
			if( $(el).attr('data-for') == fylke_name )
			{
				$(el).show();
			}
		});
	}

	$options.parent().val('');
});


// Cookie handling
function setCookie(cname, cvalue, exdays)
{
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires;
}

function getCookie(cname)
{
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++)
	{
		var c = ca[i];
		while(c.charAt(0)==' ')
		{
			c = c.substring(1);
		}
		if(c.indexOf(name) == 0)
		{
			return c.substring(name.length,c.length);
		}
	}
	return "";
}

function scrollWindowTo( element )
{
	var pos = element.offset().top - 30;

	if( $(window).width() >= 1094 )
	{
		pos -= $('header').outerHeight();
	}

	$('html, body').animate({ scrollTop: pos }, 500);
}